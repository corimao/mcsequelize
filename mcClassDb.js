/**
 * Created by jmb on 24/07/2017.
 */

"use strict";
let path = require('path');
let mcClassDbSequelize = require(path.join(mcApp.path, '/lib/mcCore/mcSequelize/mcClassDbSequelize'));

function mcClassDb (pOption) {
    let errMsgDbVide = 'db vide !!!';
    this.SVC_Nom = 'mcClassDb';
    let l_nomCnx = mcClassDbSequelize.getNomCnx(pOption.NameModule);                       // Récupere le nom du module
    let db = mcApp.mcBdd[l_nomCnx].db[pOption.Model] || undefined;
    let l_Objet = this;

    this.getWhere = function(pValue){
        let oWhere = {};
        oWhere[db.primaryKeyAttribute] = pValue;
        return oWhere;
    };

    this.insert = function(oStructure, pCallback) {
        if (db == '') {if (pCallback) {pCallback({err: errMsgDbVide})}}
        if (typeof pCallback == 'undefined') {
            MC_Tools.log("[E]" + l_Objet.SVC_NOM + " insert sans callback !!! " );
            return;
        }

        db.create(oStructure).then(function (result) {
            pCallback(null, {id: result.numero})
        }).catch(function (e) {
            pCallback({err: e})
        });
    };

    this.update = function(oStructure, pWhere, pCallback) {
        if (db == '') {if (pCallback) {pCallback({err: errMsgDbVide})}}
        if (typeof pCallback == 'undefined') {
            MC_Tools.log("[E]" + l_Objet.SVC_NOM + " update sans callback !!! " );
            return;
        }

        db.update(oStructure, {
            where: pWhere
        }).then(function (result) {
            pCallback(null, {id: result.numero})
        }).catch(function (e) {
            pCallback({err: e})
        });
    };

    this.select = function(pWhere, pCallback){
        if (db == '') {if (pCallback) {pCallback({err: errMsgDbVide})}}
        if (typeof pCallback == 'undefined') {
            MC_Tools.log("[E]" + l_Objet.SVC_NOM + " select sans callback !!! " );
            return;
        }

        db.findAll({
            where: pWhere
        }).then((result) => {
                pCallback(null, result);
            }).catch((e) => {
                pCallback({err: e})
        });

    };

    this.logging = function(pEtat){
        if (pEtat) {db.sequelize.options.logging = pEtat}
    }
}
module.exports = mcClassDb;