
"use strict";

let mcClassSequelize = require('./mcClassSequelize');
let mcClassDbSequelize = require ("./mcClassDbSequelize");

let async = require('async');
let path = require('path');

/**
 * Class mcClassRouter
 * @extends mcClassSequelize
 * @author Created by jmb on 19/09/2016. <jm.barbou@corimao.fr>
 * @see {@link http://127.0.0.1:9251/api/#api-mcClassRouter/api|http://127.0.0.1:9251/api }
 */
class mcClassRouter extends mcClassSequelize{
    /**
     * @param {object} pOption - {modelName: Nom de model db, router: route, viewRender: Vue par defaut, name: __filename}
     */
    constructor(pOption) {
        super(pOption.modelName);
        this.router = pOption.router;                                                                   // Public
        this.viewRender = pOption.viewRender;
        this.title = pOption.modelName;
        this.nameModule = path.parse(pOption.name).name;                                                // Extrait le name du module (fichier .js)
        this.mcBdd = undefined;
    }

    /**
     * mcAutentification
     * @param req {object} req - request object
     * @param res {object} res - response object
     * @param next {object} next - object suivant
     */
    mcAutentification(req, res, next) {
        // console.log('Authentification');
        next();
    };


    /**
     * init_Db
     * @param pNameCnx {string} nom de la connexion dans le fichier de configuration
     * @param req {object} req - request object
     */
    init_Db(pNameCnx, req){
        let self = this;
        self.mcBdd = mcApp.mcBdd[pNameCnx].db || undefined;                                 //
        if (self.mcBdd){
            self.initialisation(self.mcBdd, req);
        }
    };

    /**
     * initRoute
     * @param {object} pOption - {dirname: Chemin}
     * @description Initalisation des routes generiques
     */
    initRoute(pOption) {
        let self = this;

        if (pOption){
            mcApp.hbs.registerPartials(path.join(pOption.dirname, '../views'));
            // hbs.registerHelper("equal", require("handlebars-helper-equal"));
            // hbs.registerHelper('json', function(context) {return JSON.stringify(context)});                     // Utiliser pour pourvoir récuperer un object hbs (mcParam)
        }

        /**
         * router.all
         * Passage Obligatoire pour chaque route
         */
        this.router.all("*", this.mcAutentification, function (req, res, next) {
            if (self.mcBdd == undefined){
                let l_nomCnx = mcClassDbSequelize.getNomCnx(self.nameModule);                       // Récupere le nom du module
                self.init_Db(l_nomCnx, req);
                // self.mcBdd = mcApp.mcBdd[l_nomCnx].db || undefined;                                 //
            }

            // if (self.mcBdd){
            //     self.initialisation(self.mcBdd, req);
            // }
            next();
        });

        /**
         * @api {get} /xxxx Requête generiques (get)
         * @apiVersion 0.1.0
         * @apiName Get
         * @apiGroup mcClassRouter
         *
         * @apiParam {Numerique} offset Decalage de la recherche SQL.
         * @apiParam {Numerique} limit Limite le nombre de la recherche SQL.
         * @apiParam {String} order Nom de la colonne de trie.
         * @apiParamExample {json} Request-Example:
         *      /clients?offset=10&limit=100&order=Nom
         *
         * @apiSuccessExample Success-Response:
         *     HTTP/1.1 200 OK
         *     {
         *       "firstname": "John",
         *       "lastname": "Doe"
         *     }
         *
         * @apiError UserNotFound The id of the User was not found.
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 404 Not Found
         *     {
         *       "error": "UserNotFound"
         *     }
         */
        this.router.get('/', function (req, res, next) {
            var options = self.getOptions(req);

            self.db.findAll(options.Querying)
                .then(function (datas) {
                    switch (self.viewRender){
                        case 'JSON' :
                            res.json(datas);
                            break;
                        default     :
                            res.render(self.viewRender, {
                                title: self.title,
                                options: options,
                                data: datas
                            });
                            break;
                    }
                }).catch(function (e) {
                res.status('403').send(e);
            });
        });

        /**
         * @api {post} /xxxxx/create Création générique
         * @apiVersion 0.1.0
         * @apiName create
         * @apiGroup mcClassRouter
         * @apiParam  create Création générique
         *
         * @apiSuccessExample Success-Response:
         *     HTTP/1.1 201 OK
         *
         * @apiError UserNotFound The id of the User was not found.
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 409 Not Found
         *     {
         *       "error": "UserNotFound"
         *     }
         */
        this.router.post('/create', function (req, res, next) {
            var oStructure = self.loadStructure(req);

            self.db.create(oStructure).then(function() {
                res.status('201').send(oStructure);
            }).catch(function (e) {
                res.status('409').send(e);
            });
        });

        /**
         * @api {post} /xxxxx Requête post génériques (post)
         * @apiVersion 0.1.0
         * @apiName post
         * @apiGroup mcClassRouter
         * @apiParam  Attente
         *
         * @apiSuccessExample Success-Response:
         *     HTTP/1.1 201 OK
         *
         * @apiError Erreur.
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 403 Not Found
         */
        this.router.post('/', function (req, res, next) {
            var l_viewRender = self.viewRender;
            var options = self.getOptions(req);
            if (options){
                var optQuerying = options.Querying;
                var optForms = options.Forms;
                if (optForms.view){l_viewRender = optForms.view}
            }

            async.series({
                    one: function(callback){
                        // Récupère le nombre global d'enregistrement
                        self.getCount(function(counts){
                            callback(null, counts);
                        });
                    },
                    two: function(callback){
                        // Récupère les datas et le count filtré
                        self.db.findAndCountAll(optQuerying)
                            .then(function (result) {
                                callback(null, result);
                            }).catch(function (e) {
                            callback(e, null);
                        });
                    }
                },function(err, results) {
                    if(err){
                        res.status('403').send(err);
                    }else{
                        res.render(l_viewRender, {
                            title: self.title,
                            recordsTotal : results.one,
                            recordsFiltered: results.two.count,
                            data: results.two.rows,
                            options : options
                        });
                    }
                }
            );
        });

        /**
         * @api {put} /xxxxx/:id Update génériques
         * @apiVersion 0.1.0
         * @apiName put
         * @apiGroup mcClassRouter
         * @apiParam  {Numerique} id identifiant unique pour l'update
         *
         * @apiSuccessExample Success-Response:
         *     HTTP/1.1 200 OK
         *
         * @apiError Erreur.
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 404 id non valide
         */
        this.router.put('/:id', function (req, res, next) {
            var oStructure = self.loadStructure(req);

            if (req.params.id){
                self.db.update(oStructure,{
                    where: self.whereId(req)
                }).then(function() {
                    res.status('200').send();
                }).catch(function (e) {
                    res.status('404').send(e);
                });
            }else{
                res.status('404').send({err:'id non valide'})
            }
        });

        /**
         * @api {delete} /xxxxx/:id Suppression Génériques
         * @apiVersion 0.1.0
         * @apiName delete
         * @apiGroup mcClassRouter
         * @apiParam  {Numerique} id identifiant unique pour l'update
         *
         * @apiSuccessExample Success-Response:
         *     HTTP/1.1 200 OK
         *
         * @apiError Erreur.
         *
         * @apiErrorExample Error-Response:
         *     HTTP/1.1 404 id non valide
         */
        this.router.delete('/:id', function (req, res, next) {
            if (req.params.id){
                self.db.destroy(
                    {where: self.whereId(req)
                    }).then(function() {
                    res.status('200').send();
                }).catch(function (e) {
                    res.status('404').send(e);
                });
            } else{
                res.status('404').send({err:'id non valide'})
            }
        });
    }

};

module.exports = mcClassRouter;