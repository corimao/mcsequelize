/**
 * mcTabSequelize
 * @module mcTabSequelize
 * @requires mcClassDbSequelize
 * @author Created by jmb on 19/10/2016. <jm.barbou@corimao.fr>
 */

"use strict";
let mcClassDbSequelize = require ("./mcClassDbSequelize");

let mcTabSequelize = function mcStart(pOption, pCallback) {
    let self = this;
    self.TabDB = new Object();

    pOption.mcBdd.forEach(function (pModule) {
        let sequelize = new mcClassDbSequelize(pModule.db_config);
        self.TabDB[pModule.NomCnx] = sequelize;
        sequelize.authenticate().then((result) => {
            MC_Tools.log("[I]Sequelize Connexion  : " + pModule.NomCnx +" > Ok ");
        }).catch((e) => {
            MC_Tools.log("[E]Sequelize Connexion  : " + pModule.NomCnx +" > NOk "+ e);
        });
    });
    mcApp.mcBdd = self.TabDB;
    if (pCallback){pCallback()}
};

module.exports = mcTabSequelize;
