
"use strict";

/**
 * mcClassSequelize
 * @author Created by jmb on 19/09/2016. <jm.barbou@corimao.fr>
 */
class mcClassSequelize{
    /**
     * @param {strings} pModelName - Nom du models
     */
    constructor (pModelName) {
        this.modelName = pModelName;
        this.db = undefined;
    }

    /**
     * initialisation
     * @protected
     * @param pDb
     * @param pReq
     */
    initialisation(pDb, pReq){
        this.db = pDb[this.modelName];
        if (pReq){
            this.baseUrl = pReq.baseUrl;
        }
    }

    /**
     * @methode getDb
     * @private
     * @returns {objet} {nameId: '', baseUrl:''}
     * @description Renvoi un object contenant le nom du chanmp id et l'url pour l'atteindre
     */
    _getDb(){
        let optDb = new Object();
        optDb.nameId = this.db.primaryKeyAttribute;
        optDb.baseUrl = this.baseUrl;
        return optDb;
    }

    /**
     * whereId
     * @param {Object} express
     * @returns {Object} l'object {id: value}
     * @description Renvoi un object contenant la primary key et sa valeur
     */
    whereId(req){
        let oWhere = new Object();
        oWhere[this.db.primaryKeyAttribute] = parseInt(req.params.id) || false ;
        return oWhere;
    };

    /**
     * loadStructure
     * @param {Object}  express
     * @returns {Object}
     * @description Chargement de la structure depuis la requête express
     */
    loadStructure(req){
        let oStructure = new Object();
        Object.keys(this.db.tableAttributes).forEach(function (attribute) {
            oStructure[attribute] = req.body[attribute]
        });
        return oStructure;
    };

    /**
     * optQuerying
     * @param {Object} express express
     * @returns {Object} {offset: offset, limit: limit, order: order, attributes: attributes, where: where}
     * @description renvoi un objet contenant {offset: offset, limit: limit, order: order, attributes: attributes, where: where}
     */
    optQuerying(req){

        let offset = req.body.offset || req.query.offset;                                               // Partie Offset
        if (offset){offset = parseInt(offset)}

        let limit = req.body.limit || req.query.limit;                                                  // Partie Limit
        if (limit){limit = parseInt(limit)}

        let order = req.body.order || req.query.order;                                                  // Partie Order

        let attributes = req.body.attributes || req.query.attributes;                                   // Partie Attribut
        if (attributes){attributes = attributes.split(',')}

        // Partie where
        let where = {};
        if (req.body.where) {
            where = mcApp.mcTools.jsonParse(req.body.where);                                        //{NomFam: {$like: 'A%'}};
        }

        let repOptions = {};
        if (req.body.options) {
            repOptions = mcApp.mcTools.jsonParse(req.body.options);
        }

        let options = {offset: offset, limit: limit, order: order, attributes: attributes, where: where};

        if (repOptions !=={}) {
            options = Object.assign(options, repOptions)
        }

        return options;
    };

    /**
     * optForms
     * @param {Object} express
     * @returns {Object} {"view":"mcVueSelect","attributs":[{"champ":"NomFam","taille":"8","visible":"true"}],"retour":"C_FamPiece"}
     * @description Recuperation des options des forms
     */
     optForms(req){
        let optForm = undefined;
        if (req.body.form){optForm =  mcApp.mcTools.jsonParse(req.body.form)}
        return optForm;
    };

    /**
     * getOptions
     * @protected
     * @param {Object} express
     * @returns {Object}
     */
    getOptions(req){
        let options = new Object();
        options.Querying = this.optQuerying(req);
        options.Forms = this.optForms(req);
        options.db = this._getDb();

        return options;
    };

    /**
     * getCount
     * @param {object} optQuerying Option de filtre pour le count (facultatif)
     * @param {function} retour
     */
    getCount(optQuerying, pCallBack){
        if (typeof optQuerying == "function") {pCallBack = optQuerying}         // Dans le cas de optQuerying facultatif
        let oResult = undefined;
        this.db.count(optQuerying)
            .then(function (result) {
                if (pCallBack){pCallBack(result)}
                return null;
            }).catch(function (e) {
                mcApp.mcTools.log('mcSequelize.count: ', e);
                if (pCallBack){pCallBack()}
        });
    };
};

module.exports = mcClassSequelize;