"use strict";

let fs = require("fs");
let path = require("path");
let Sequelize = require("sequelize");
let env = process.env.NODE_ENV || "development";

/**
 * Class mcClassDbSequelize
 * @extends Sequelize
 * @author Created by jmb on 19/09/2016. <jm.barbou@corimao.fr>
 */
class mcClassDbSequelize extends Sequelize{
    constructor(pOption) {
        super(pOption.database, pOption.username, pOption.password, pOption);
        this.TabModule = new Array();
        this.db = {};
        this.initialisation();
    }

    /**
     * loadModuleCore
     * @returns {array}
     * @description Charge les modelData des modules core
     */
    static loadModuleCore(){
        let TabModule = Array();

        mcApp.mcPrefs.mcModules.core.forEach(function (pModule) {
            if ((pModule.actif) && (pModule.modelData)) {
                TabModule.push(pModule);                                              // Ajoute le module
            }
        });
        return TabModule;
    };

    /**
     * loadModuleOption
     * @returns {array}
     * @description Charge les modelData des modules optionnels
     */
    static loadModuleOption(){
        let TabModule = Array();

        mcApp.mcPrefs.mcModules.option.forEach(function (pModule) {
            if ((pModule.actif) && (pModule.modelData)) {
                TabModule.push(pModule);                                              // Ajoute le module
            }
        });
        return TabModule;
    };

    /**
     * loadModule
     * @returns {array}
     * @description Chargement des modules core et optionnel
     */
    static loadModule(){
        let TabCore = mcClassDbSequelize.loadModuleCore();
        let TabOption = mcClassDbSequelize.loadModuleOption();

        return TabCore.concat(TabOption);                                       // Renvoi un tableau contenant tous les modules core + Option
    };

    /**
     * loadModelData
     * @description Chargement de modelData
     */
    loadModelData(){
        let self = this;

        self.TabModule.forEach(function (pModule) {
            let l_chemin = path.join(mcApp.path, pModule.url, '/models');
            fs
                .readdirSync(l_chemin)
                .filter(function (file) {
                    return (file.indexOf(".") !== 0) && (file !== "index.js");
                })
                .forEach(function (file) {
                    let l_File = path.join(l_chemin, file);
                    let model = self.import(l_File);
                    self.db[model.name] = model;
                });
        });
    };

    /**
     * loadModelAssociate
     * @description Chargement des model assiociate
     */
    loadModelAssociate(){
        let self = this;

        Object.keys(self.db).forEach(function (modelName) {
            if ("associate" in self.db[modelName]) {
                self.db[modelName].associate(self.db);
            }
        });
    };


    /**
     * initialisation
     * @description initalisation de l'object
     */
    initialisation(){
        this.TabModule = mcClassDbSequelize.loadModule();
        this.loadModelData();
        this.loadModelAssociate();
    };

    /**
     * getNomCnx
     * @param pNameModule
     * @returns {string}
     * @descriptif cherche et renvoi le Nom de la Connexion (NomCnx)
     */
    static getNomCnx(pNameModule){
        let l_NomCnx = '';
        let l_TabModule = mcClassDbSequelize.loadModule();

        l_TabModule.forEach(function (pModule) {
            if (pModule.name == pNameModule){
                l_NomCnx = pModule.option.NomCnx
            }
        });
        return l_NomCnx;
    };

}

module.exports = mcClassDbSequelize;